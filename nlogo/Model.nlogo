__includes["Code.nls"]
@#$#@#$#@
GRAPHICS-WINDOW
230
10
743
524
-1
-1
5.0
1
10
1
1
1
0
1
1
1
0
100
0
100
1
1
1
ticks
30.0

BUTTON
10
10
110
50
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
115
10
215
50
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

SLIDER
10
62
215
95
beta
beta
0
1
0.2
0.01
1
NIL
HORIZONTAL

SLIDER
10
95
215
128
ticks-infected
ticks-infected
1
10
2.0
1
1
NIL
HORIZONTAL

SLIDER
10
155
215
188
start-dispersal
start-dispersal
0
11
8.0
1
1
NIL
HORIZONTAL

SLIDER
10
190
215
223
length-dispersal
length-dispersal
1
11
3.0
1
1
NIL
HORIZONTAL

SLIDER
10
250
215
283
dispersal-radius
dispersal-radius
1
5
3.0
0.5
1
NIL
HORIZONTAL

SLIDER
10
285
215
318
num-offspring
num-offspring
0
10
4.0
1
1
NIL
HORIZONTAL

PLOT
10
345
215
520
Count by status
time [months]
# foxes
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"S" 1.0 0 -14070903 true "" "plot count patches with [state = S]"
"I" 1.0 0 -5298144 true "" "plot count patches with [state = I]"
"R" 1.0 0 -14439633 true "" "plot count patches with [state = R]"

@#$#@#$#@
## WHAT IS IT?

## HOW IT WORKS

## HOW TO USE IT
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250
@#$#@#$#@
NetLogo 6.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
